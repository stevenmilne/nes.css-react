import React from 'react';
import { storiesOf, addDecorator } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Container, Button } from '../../dist/index'

const containerStyle = {
  width: '22rem',
  marginLeft: 'auto',
  marginRight: 'auto',
  marginTop: '50px',
  marginBottom: '50px'
}

const style = {
	display: 'block',
	width: '16rem',
	marginTop: '10px',
	marginLeft: 'auto',
  marginRight: 'auto'
}

const story = storiesOf('Button', module)
story.addDecorator(storyFn => <div style={containerStyle}>{storyFn()}</div>)

story.add('all', () => (
	<Container title="All Buttons">
		<Button style={style}>Default</Button>
		<Button style={style} primary>Primay</Button>
		<Button style={style} success>Success</Button>
		<Button style={style} warning>Warning</Button>
		<Button style={style} error>Error</Button>
		<Button style={style} disabled>Disabled</Button>
	</Container>
))
