import React from 'react';
import { storiesOf, addDecorator } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Container, Checkbox, Input, Radio, Select, TextArea, Text, Button } from '../../dist/index'

const style = {
  width: '75%',
  marginLeft: 'auto',
  marginRight: 'auto',
  marginTop: '50px',
  marginBottom: '50px'
}

const decorateCenter = storyFn => <div className="storybook-decorator-center">{storyFn()}</div>

const options = [{
  name: 'Select...',
  value: '',
  disabled: true,
  selected: true,
  hidden: true
}, {
  name: 'First Option',
  value: '1',
}, {
  name: 'Second Option',
  value: '2'
}, {
  name: 'Third Option',
  value: '3'
}]

storiesOf('Form', module)
  .add('Example', () => (
    <Container title="New Account" style={style}>
      <form>
        <Text>Fill in your details below and click the 'Continue' button at the bottom to create your account.</Text>
        <br /><br />
        <Input id="forename" label="First Name" />
        <br />
        <Input id="surname" label="Last Name" />
        <br />
        <Input type="email" id="email" label="Email Address" />
        <br />
        <Text>Gender</Text>
        <br />
        <Radio id="gender" label="Male" checked />
        <Radio id="gender" label="Female" />
        <br />
        <div style={{ minHeight: '50px' }}>
          <div style={{ position: 'absolute', right: '2rem' }}>
            <Button type="reset" style={{ marginRight: '1rem' }}>Reset</Button>
            <Button type="submit" primary>Continue</Button>
          </div>
        </div>
      </form>
    </Container>
  ))
  .add('Select', () => (
    <Container title="Select Modifiers" style={style}>
      <Select id="normalSelect" label="Normal" options={options} />
      <Select id="successSelect" label="Success" options={options} success />
      <Select id="warningSelect" label="Warning" options={options} warning />
      <Select id="errorSelect" label="Error" options={options} error />
      <Select id="disabledSelect" label="Disabled" options={options} disabled />
    </Container>
  ))
  .add('TextArea', () => (
    <Container style={style}>
      <TextArea id="textarea" label="Text Area">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fringilla tellus orci, lobortis malesuada diam fringilla vel. Curabitur suscipit in ligula quis porttitor. Curabitur ac eleifend urna, in gravida justo. Suspendisse id libero non ipsum interdum sollicitudin sit amet tempus felis. Duis euismod justo non neque molestie, pretium interdum ipsum mollis. Integer venenatis consequat porta. Proin ac semper dui. Suspendisse et urna convallis, egestas augue eu, maximus nibh. Maecenas maximus metus et ex luctus suscipit. Donec consequat est ex, vel tincidunt dolor feugiat non. Mauris semper urna erat, sed varius mi sollicitudin eu.
      </TextArea>
    </Container>
  ))

storiesOf('Form/Input', module)
  .add('Types', () => (
    <Container title="Input Types" style={style}>
      <Input type="text" id="textInput" label="Text" />
      <Input type="date" id="dateInput" label="Date" />
      <Input type="email" id="emailInput" label="Email" />
      <Input type="file" id="fileInput" label="File" />
      <Input type="month" id="monthInput" label="Month" />
      <Input type="number" id="numberInput" label="Number" />
      <Input type="password" id="passwordInput" label="Password" />
      <Input type="range" id="rangeInput" label="Range" />
      <Input type="tel" id="telInput" label="Telephone" />
      <Input type="time" id="timeInput" label="Time" />
      <Input type="url" id="urlInput" label="URL" />
      <Input type="week" id="weekInput" label="Week" />
    </Container>
  ))
  .add('Modifiers', () => (
    <Container title="Input Modifiers" style={style}>
      <Input id="normalInput" label="Normal" />
      <Input id="successInput" label="Success" success />
      <Input id="warningInput" label="Warning" warning />
      <Input id="errorInput" label="Error" error />
      <Input id="disabledInput" label="Disabled" disabled />
    </Container>
  ))

storiesOf('Form/Checkbox', module)
  .addDecorator(decorateCenter)
  .add('normal', () => (
  	<Container title="Checkbox">
  		<Checkbox id="exampleCheckbox1" label="Check me" checked />
      <br/>
      <Checkbox id="exampleCheckbox2" label="No, check me" />
  	</Container>
  ))
  .add('dark', () => (
    <Container title="Dark Checkbox" dark>
      <Checkbox id="exampleCheckbox1" label="Check me" checked dark />
      <br/>
      <Checkbox id="exampleCheckbox2" label="No, check me" dark />
    </Container>
  ));

storiesOf('Form/Radio', module)
  .addDecorator(decorateCenter)
  .add('normal', () => (
  	<Container title="Radio">
  		<Radio id="exampleRadio1" label="Check me" checked />
      <br/>
      <Radio id="exampleRadio1" label="No, check me" />
  	</Container>
  ))
  .add('dark', () => (
    <Container title="Dark Radio" dark>
      <Radio id="exampleRadio2" label="Check me" checked dark />
      <br/>
      <Radio id="exampleRadio2" label="No, check me" dark />
    </Container>
  ));
