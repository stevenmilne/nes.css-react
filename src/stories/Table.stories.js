import React from 'react';
import { storiesOf, addDecorator } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Container, Table, TableHead, TableBody } from '../../dist/index'

const containerStyle = {
  width: '35rem',
  marginLeft: 'auto',
  marginRight: 'auto',
  marginTop: '50px',
  marginBottom: '50px'
}

const story = storiesOf('Table', module)
story.addDecorator(storyFn => <div style={containerStyle}>{storyFn()}</div>)

story.add('default', () => (
	<Container title="Default Table">
		<Table>
			<TableHead>
				<th>Column 1</th>
				<th>Column 2</th>
				<th>Column 3</th>
			</TableHead>
			<TableBody>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
			</TableBody>
		</Table>
	</Container>
))

story.add('bordered', () => (
	<Container title="Bordered Table">
		<Table bordered>
			<TableHead>
				<th>Column 1</th>
				<th>Column 2</th>
				<th>Column 3</th>
			</TableHead>
			<TableBody>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
			</TableBody>
		</Table>
	</Container>
))

story.add('centered', () => (
	<Container title="Centered Table">
		<Table centered>
			<TableHead>
				<th>Column 1</th>
				<th>Column 2</th>
				<th>Column 3</th>
			</TableHead>
			<TableBody>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
			</TableBody>
		</Table>
	</Container>
))

story.add('dark', () => (
	<Container title="Dark Table" dark>
		<Table dark>
			<TableHead>
				<th>Column 1</th>
				<th>Column 2</th>
				<th>Column 3</th>
			</TableHead>
			<TableBody>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
			</TableBody>
		</Table>
	</Container>
))

story.add('dark with border', () => (
	<Container title="Dark Table with Border" dark>
		<Table dark bordered>
			<TableHead>
				<th>Column 1</th>
				<th>Column 2</th>
				<th>Column 3</th>
			</TableHead>
			<TableBody>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
				<tr>
					<td>Value 1</td>
					<td>Value 2</td>
					<td>Value 3</td>
				</tr>
			</TableBody>
		</Table>
	</Container>
))
