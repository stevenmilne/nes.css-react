import React from 'react';
import { storiesOf, addDecorator } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Container, Text, Button, Input } from '../../dist/index'

const story = storiesOf('Container', module)
story.addDecorator(storyFn => <div className="storybook-decorator-center">{storyFn()}</div>);

story.add('default', () => (
  <Container>
    <p>This is some content</p>
  </Container>
))

story.add('dark', () => (
  <Container dark>
    <p>This is some content</p>
  </Container>
))

story.add('with title', () => (
  <Container title="With Title">
    <p>This is some content</p>
  </Container>
))

story.add('dark with title', () => (
  <Container title="With Title" dark>
    <p>This is some content</p>
  </Container>
))

story.add('centered', () => (
  <Container centered>
    <p>This is some content</p>
  </Container>
))

story.add('dark centered', () => (
  <Container dark centered>
    <p>This is some content</p>
  </Container>
))

story.add('with title and centered', () => (
  <Container title="With Title and Centered" centered>
    <p>This is some content</p>
  </Container>
))

story.add('dark with title and centered', () => (
  <Container title="With Title and Centered" dark centered>
    <p>This is some content</p>
  </Container>
))

export default story
