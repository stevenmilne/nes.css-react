import React from 'react';
import { storiesOf, addDecorator } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Container, List } from '../../dist/index'

const containerStyle = {
  width: '22rem',
  marginLeft: 'auto',
  marginRight: 'auto',
  marginTop: '50px',
  marginBottom: '50px'
}

const story = storiesOf('List', module)
story.addDecorator(storyFn => <div style={containerStyle}>{storyFn()}</div>)

story.add('disc', () => (
	<Container title="Disc List">
		<List disc>
			<li>Item 1</li>
			<li>Item 2</li>
			<li>Item 3</li>
		</List>
	</Container>
))

story.add('circle', () => (
	<Container title="Circle List">
		<List circle>
			<li>Item 1</li>
			<li>Item 2</li>
			<li>Item 3</li>
		</List>
	</Container>
))
