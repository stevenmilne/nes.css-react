
export interface FieldModifiers {
  success?: boolean
  warning?: boolean
  error?: boolean
  disabled?: boolean
}

export interface Modifiable extends FieldModifiers {
  primary?: boolean
}
