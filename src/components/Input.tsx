import React from 'react'
import classNames from 'classnames'

import { FieldModifiers } from '../types'

export interface InputProps extends FieldModifiers {
  id: string
  type?: string
  name?: string
  label: string
  disabled?: boolean
  inline?: boolean
  className?: string
}

const Input: React.FunctionComponent<InputProps> = ({
  className,
  children,
  disabled,
  success,
  warning,
  inline,
  error,
  label,
  id,
  type = 'text',
  name = label,
  ...others
}) => (
  <div
    className={classNames('nes-field', {
      'is-inline': inline
    })}
  >
    <label htmlFor={id}>{label}</label>
    <input
      id={id}
      type={type}
      name={name}
      disabled={disabled}
      aria-disabled={disabled}
      className={classNames('nes-input', className, {
        'is-success': success,
        'is-warning': warning,
        'is-error': error,
        'is-disabled': disabled
      })}
      {...others}
    />
  </div>
)

export default Input
