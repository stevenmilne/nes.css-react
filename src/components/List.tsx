import React from 'react'
import classNames from 'classnames'

export interface ListProps {
	disc?: boolean
	circle?: boolean
	items?: any[]
}

const List: React.FunctionComponent<ListProps> = ({ disc = true, circle = false, items, children }) => (
	<ul
		className={classNames('nes-list', {
			'is-disc': disc,
			'is-circle': circle
		})}
	>
		{items && items.map(item => <li>{item}</li>)}
		{children}
	</ul>
)

export default List
