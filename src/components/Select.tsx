import React from 'react'
import classNames from 'classnames'

import { FieldModifiers } from '../types'

export interface SelectOptions {
  name: string
  value: string
  disabled?: boolean
  selected?: boolean
  hidden?: boolean
}

interface SelectProps extends FieldModifiers {
  id: string
  label: string
  name?: string
  options: SelectOptions[]
  className?: string
}

const Select: React.FunctionComponent<SelectProps> = ({ id, label, name = id, options, className, success, warning, error, disabled, ...others }) => (
  <>
    <label htmlFor={id}>{label}</label>
    <div
      className={classNames('nes-select', {
        'is-success': success,
        'is-warning': warning,
        'is-error': error,
        'is-disabled': disabled
      })}
      >
      <select id={id} name={name} className={classNames(className)} disabled={disabled} {...others}>
        {options.map(({ name, value, disabled = false, selected = false, hidden = false }) => (
          <option value={value} disabled={disabled} selected={selected} hidden={hidden}>{name}</option>
        ))}
      </select>
    </div>
  </>
)

export default Select
