import React from 'react'
import classNames from 'classnames'

import { Modifiable } from '../types'

export interface TextProps extends Modifiable {
  className?: string
}

const Text: React.FunctionComponent<TextProps> = ({ children, primary, success, warning, error, disabled, className }) => (
  <span
    className={classNames('nes-text', className, {
      'is-primary': primary,
      'is-success': success,
      'is-warning': warning,
      'is-error': error,
      'is-disabled': disabled
    })}
  >
    {children}
  </span>
)

export default Text
