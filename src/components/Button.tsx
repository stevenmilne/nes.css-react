import React from 'react'
import classNames from 'classnames'

import { Modifiable } from '../types'

export interface ButtonProps extends Modifiable {
  onClick?: (event: React.MouseEvent) => void
  className?: string
}

const Button: React.FunctionComponent<ButtonProps> = ({ primary, success, warning, error, disabled, onClick, children, className, ...others }) => (
  <button
    type="button"
    disabled={disabled}
    aria-disabled={disabled}
    onClick={onClick}
    {...others}
    className={classNames('nes-btn', className, {
      'is-primary': primary,
      'is-success': success,
      'is-warning': warning,
      'is-error': error,
      'is-disabled': disabled
    })}
  >
    {children}
  </button>
)

export default Button
