import React from 'react'

interface TextAreaProps {
  id: string
  label: string
  name?: string
}

const TextArea: React.FunctionComponent<TextAreaProps> = ({ id, label, name = id, children, ...others }) => (
  <div>
    <label htmlFor={id}>{label}</label>
    <textarea id={id} name={name} className="nes-textarea" {...others}>{children}</textarea>
  </div>
)

export default TextArea
