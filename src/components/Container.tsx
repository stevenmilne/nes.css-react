import React from 'react'
import classNames from 'classnames'

export interface ContainerProps {
  centered?: boolean
  dark?: boolean
  title?: string
  rounded?: boolean
  className?: string
}

const Container: React.FunctionComponent<ContainerProps> = ({ children, centered, dark, title, rounded, className, ...others }) => (
  <div
    {...others}
    className={classNames('nes-container', className, {
      'is-centered': centered,
      'is-dark': dark,
      'with-title': title,
      'is-rounded': rounded
    })}
  >
    {title ? <p className="title">{title}</p> : null}
    {children}
  </div>
)

export default Container
