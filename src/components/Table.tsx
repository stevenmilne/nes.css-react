import React from 'react'
import classNames from 'classnames'

export interface TableProps {
	bordered?: boolean
	centered?: boolean
	dark?: boolean
}

const Table: React.FunctionComponent<TableProps> = ({ bordered = false, centered = false, dark = false, children }) => (
	<div className="nes-table-responsive">
		<table
			className={classNames('nes-table', {
				'is-bordered': bordered,
				'is-centered': centered,
				'is-dark': dark
			})}
		>
			{children}
		</table>
	</div>
)

export const TableHead: React.FunctionComponent = ({ children }) => (
	<thead>
		<tr>
			{children}
		</tr>
	</thead>
)

export const TableBody: React.FunctionComponent = ({ children }) => (
	<tbody>
		{children}
	</tbody>
)

export default Table
