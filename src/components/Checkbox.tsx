import React from 'react'
import classNames from 'classnames'

interface CheckboxProps {
  id: string
  label: string
  name?: string
  className?: string
  dark?: boolean
}

const Checkbox: React.FunctionComponent<CheckboxProps> = ({ id, name = id, className, dark, label, ...others }) => (
  <label>
    <input
      type="checkbox"
      id={id}
      name={name}
      {...others}
      className={classNames('nes-checkbox', className, {
        'is-dark': dark
      })}
    />
    <span>{label}</span>
  </label>
)

export default Checkbox
