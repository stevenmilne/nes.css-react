import React from 'react'
import classNames from 'classnames'

interface RadioProps {
	id: string
	label: string
	name?: string
	className?: string
	dark?: boolean
}

const Radio: React.FunctionComponent<RadioProps> = ({ id, name = id, label, className, dark, ...others}) => (
	<label>
		<input
			type="radio"
			id={id}
			name={name}
			className={classNames('nes-radio', className, {
				'is-dark': dark
			})}
			{...others}
		/>
		<span>{label}</span>
	</label>
)

export default Radio
