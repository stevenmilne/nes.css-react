# NES.css React Component Library
This repository contains a component library for react based on the [NES.css](https://nostalgic-css.github.io/NES.css/) CSS framework.

## License
This project is released under the MIT license as-is NES.css. You can check the license information in the `LICENSE` file in this repository.
